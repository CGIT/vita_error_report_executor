# This script was developed by the Center for Geospatial Information Technology at Virginia Tech

import cgit_logging
import os
import pandas
import arcpy
import zipfile
import ScriptMessaging  # This version of SciptMessaging is written for use with a GMAIL account.  Edit the SMTP_SSL settings to change the server type.
# import username and password for email credentials 
import __credentials

# required input parameters 
RCL_parameter = r"C:\...\road_centerlines"  # path to the rcl feature class
AP_parameter = r"C:\...\address_points"  # path to the address point feature class
msag_directory = r"C:\...\MSAG"  # path to a folder containing the msag data for each jurisdiction
ali_directory = r"C:\...\MSAG"  # path to a folder containing the ali data for each jurisdiction
email_username = __credentials.username
email_password = __credentials.password
email_distribution_list = ['input_email@gmail.com',]  # email list consisting of the gis managers from each jurisdiction

log = cgit_logging.root.getChild("NRVECA_Dev")
arcpy.env.overwriteOutput = True

# The execution of this workflow follows the procedures outlined in the VITA Error Report Tool documentation.
# This process requires the VITA Error Report Tools environment be set up according to the VITA documentation.
# https://vgin.maps.arcgis.com/apps/PublicGallery/index.html?appid=d5d90aedbca54b9c90df2833330d2db9

log.info("starting 0101_UpdateWorkspaceImport.py")
execfile(r"C:\VGIN\Analysis\00-ImportPrepData_Analysis\Scripts\0101_UpdateWorkspaceImport.py", {'RCL_parameter': RCL_parameter, 'AP_parameter': AP_parameter})
log.info("completed 0101_UpdateWorkspaceImport.py")

log.info("starting MontgomeryNRV.py")
execfile(r"C:\VGIN\Analysis\00-ImportPrepData_Analysis\Scripts\Localities\MontgomeryNRV.py")
log.info("completed MontgomeryNRV.py")

log.info("starting 0301_AppendToolSchema.py")
execfile(r"C:\VGIN\Analysis\00-ImportPrepData_Analysis\Scripts\0301_AppendToolSchema.py")
log.info("completed 0301_AppendToolSchema.py")

log.info("starting 0302_CleanseLocalAttributes.py")
execfile(r"C:\VGIN\Analysis\00-ImportPrepData_Analysis\Scripts\0302_CleanseLocalAttributes.py")
log.info("completed 0302_CleanseLocalAttributes.py")

log.info("starting 0303_SpatialJoinCalculateZones.py")
execfile(r"C:\VGIN\Analysis\00-ImportPrepData_Analysis\Scripts\0303_SpatialJoinCalculateZones.py")
log.info("completed 0303_SpatialJoinCalculateZones.py")

log.info("starting CivicAddressLayersAnalysis.py")
execfile(r"C:\VGIN\Analysis\CivicAddressLayersAnalysis\Scripts\CivicAddressLayersAnalysis.py")
log.info("completed CivicAddressLayersAnalysis.py")

'''
# The GIS911SynchronizationAnalysis section of this workflow was not fully developed due to the lack of ALI data
# This section merges the MSAG data from all of the jurisdictions
all_data = pandas.DataFrame()
msag_files = os.listdir(msag_directory)
for msag_file in msag_files:
    msag_data = pandas.read_excel(os.path.join(msag_directory,msag_file))
    all_data = all_data.append(msag_data, ignore_index=True)
merged_msag = os.path.join(msag_directory,'merged_msag.csv')
all_data.to_csv(merged_msag)

#ALI_parameter = r""
execfile(r"C:\VGIN\Analysis\00-ImportPrepData_Analysis\Scripts\0401_ImportParseMSAGALI_NENA02010v2.py", {'MSAG_parameter': MSAG_parameter})
execfile(r"C:\VGIN\Analysis\GIS911SynchronizationAnalysis\Scripts\GIS911SynchronizationAnalysis.py")
'''

# Create a layer file with a relative path for easy distribution 
arcpy.SaveToLayerFile_management(r"C:\VGIN\Analysis\CivicAddressLayersAnalysis\lyr\CivicAddressLayersAnalysis.lyr", r"C:\VGIN\Analysis\CivicAddressLayersAnalysis\CivicAddressLayersAnalysis.lyr", "RELATIVE")

# Zip the results in preparation to attach 
if os.path.exists("VGIN_ERROR_REPORT.zip"):
    os.remove("VGIN_ERROR_REPORT.zip")
zf = zipfile.ZipFile("VGIN_ERROR_REPORT.zip", "w")
for root, dirs, files in os.walk(r"C:\VGIN\Analysis\CivicAddressLayersAnalysis\CivicAddressLayersAnalysis.gdb"):
    for f in files:
        try:
            zf.write(os.path.join(root, f))
        except IOError:
            log.info(str(f)+" was not included in zip file due to schema lock")
            pass
zf.write(r"C:\VGIN\Analysis\CivicAddressLayersAnalysis\CivicAddressLayersAnalysis.lyr")        
zf.close()

# Creat a summary view of the results
mxd = arcpy.mapping.MapDocument(r"NRVECA_VGIN_Template.mxd")
mxd.findAndReplaceWorkspacePaths(r"C:\VGIN\Analysis\CivicAddressLayersAnalysis", r"C:\VGIN\Analysis\CivicAddressLayersAnalysis")
arcpy.mapping.ExportToPDF(mxd, r"VGIN_ERROR_REPORT.pdf")
del mxd

# Read CivicAddressLayersAnalysis_Report and insert into email body
vita_check_results = []
with arcpy.da.SearchCursor(r"C:\VGIN\Analysis\CivicAddressLayersAnalysis\CivicAddressLayersAnalysis.gdb\CivicAddressLayersAnalysis_Report", ['Check', 'Data_Rule_Violation', 'Count']) as cursor:
    for row in cursor:
        vita_check_results.append(', '.join(['Check #'+str(row[0]), row[1]+':  '+str(row[2])]))
email_check_results = '\n'.join(vita_check_results)

# Send the resuk=lts via email
email_subject = "Regional 911 VGIN Error Report"
messenger = ScriptMessaging.Messenger(email_username, email_password, email_subject, email_distribution_list)
messenger.email_message('VGIN Error Report Completed. Civic Analysis Results attached.\n\nCivic Address Layers Analysis Report:\n'+email_check_results,"VGIN Error Report",["VGIN_ERROR_REPORT.pdf", "VGIN_ERROR_REPORT.zip"])

