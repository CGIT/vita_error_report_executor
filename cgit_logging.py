# This script was developed by the Center for Geospatial Information Technology at Virginia Tech
#Insert this in a file that will be called in the main execution of the program to set up all of the log handlers and formatting
import local_settings
import logging, os, time
import logging.handlers as logging_handlers
#logger_name = "program_name" #'''Edit to match the given program you're writing'''
root = logging.getLogger(local_settings.log_rootname)
log_format_string = "%(asctime)s - %(levelname)s  - %(name)s:%(lineno)d - %(message)s"
timestamp_format_string = "%Y-%m-%dT%H:%M:%S"
log_format = logging.Formatter(log_format_string, timestamp_format_string)

#Set the logging level to NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL
log_level = local_settings.loglevel #'''Edit based on what overall levels you want logging'''
root.setLevel(getattr(logging,log_level.upper()))

handlers = {}
#This section causes the logging function to write logs to the console
stdout = logging.StreamHandler()
stdout.setFormatter(log_format)
stdout.setLevel(getattr(logging,log_level.upper())) #'''Edit to set the minimum error level to log with this particular handler'''
root.addHandler(stdout)
handlers['stream'] = stdout

#This section causes the logging function to write logs to a log file
log_file = os.path.join(local_settings.logpath, "{}.log".format(local_settings.log_rootname)) 
#fileout = logging.FileHandler(log_file)
fileout = logging_handlers.TimedRotatingFileHandler(log_file, when='d', interval=1)
fileout.setFormatter(log_format)
fileout.setLevel(getattr(logging,log_level.upper())) #'''Edit to set the minimum error level to log with this particular handler'''
root.addHandler(fileout)
handlers['file'] = fileout

#This section causes the logging function to write error logs to a dedicated error log file
log_file = os.path.join(local_settings.logpath, "{}_errors.log".format(local_settings.log_rootname)) 
errors = logging_handlers.TimedRotatingFileHandler(log_file, when='d', interval=1, delay=True)
errors.setFormatter(log_format)
errors.setLevel('ERROR')
root.addHandler(errors)



#Django logger example
LOGGING = {
    'version':1,
    'disable_existing_loggers': False,
    'formatters':{
        'details':{
            'format': '{asctime} - {levelname} - {name}:{lineno} - {message}'}},
    'handlers':{
        'file':{
            'filename':'/mnt/minnow/SourceData/logs/{name}/master/{name}.log'.format(name=''),
            'level':'INFO',
            'formatter':'details',
            'class':'logging.handlers.TimedRotatingFileHandler',
            'when':'d',
            'interval':1},
        'errors':{
            'filename':'/mnt/minnow/SourceData/logs/{name}/master/{name}_error.log'.format(name=''),
            'level':'ERROR',
            'formatter':'details',
            'class':'logging.handlers.TimedRotatingFileHandler',
            'when':'d',
            'interval':1,
            'delay':True}}
}